@extends('app')
@section('head.title')
    Đặt sân
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-12 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Đặt sân</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-block">
                <form class="forms-sample" method="post"
                      action="{{route('khach-hang-khung-gio.update',$khachHangKhungGio->id)}}">
                    {{ csrf_field() }}
                    @method('PATCH')
                    <div class="row">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="id" value="{{ $khachHangKhungGio->id }}"
                                   hidden>
                            <label class="col-form-label">Họ tên<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}"
                                    maxlength="255">
                            @if ($errors->has('name'))
                                <p class="text-danger">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Số điện thoại <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                                   maxlength="20">
                            @if ($errors->has('phone'))
                                <p class="text-danger">{{$errors->first('phone')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Email</label>
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}" maxlength="50">
                            @if ($errors->has('email'))
                                <p class="text-danger">{{$errors->first('email')}}</p>
                            @endif
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Giới tính</label>
                            <div class="form-radio">
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="gender" value="1" checked>
                                        <i class="helper"></i>Nam
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="gender" value="2">
                                        <i class="helper"></i>Nữ
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="gender" value="3">
                                        <i class="helper"></i>Khác
                                    </label>
                                </div>
                            </div>
                            @if ($errors->has('gender'))
                                <p class="text-danger">{{$errors->first('gender')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Địa chỉ</label>
                            <input type="text" class="form-control" name="address" value="{{old('address')}}"
                                   maxlength="255">
                            @if ($errors->has('address'))
                                <p class="text-danger">{{$errors->first('address')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Kiểu đặt sân</label>
                            <div class="form-radio">
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="type" value="2" checked>
                                        <i class="helper"></i>Không cố định
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="type" value="1">
                                        <i class="helper"></i>Cố định
                                    </label>
                                </div>

                            </div>
                            @if ($errors->has('type'))
                                <p class="text-danger">{{$errors->first('type')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Tên sân</label>
                            <select style="height: 34px" class="form-control form-control-sm js-select2-no-search"
                                    name="san" disabled>
                                <option value="">--- Tên sân ---</option>
                                @foreach($san as $value)
                                    <option @if($khachHangKhungGio->id_san == $value->id) selected
                                            @endif value="{{$value->id}}">{{$value['name']}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('san'))
                                <p class="text-danger">{{$errors->first('san')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Ngày đá</label>
                            <input style="height: 34px" id="search_input" type="date" class="form-control"
                                   name="active_date" value="{{$khachHangKhungGio->active_date}}" readonly>
                            @if ($errors->has('active_date'))
                                <p class="text-danger">{{$errors->first('active_date')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Khung giờ</label>
                            <select style="height: 34px" class="form-control form-control-sm js-select2-no-search"
                                    name="timeframe" disabled>
                                <option value="">--- Khung giờ ---</option>
                                @foreach($time as $value1)
                                    <option @if($khachHangKhungGio->id_khung_gio == $value1->id) selected
                                            @endif value="{{$value1->id}}">{{$value1['name']}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('timeframe'))
                                <p class="text-danger">{{$errors->first('timeframe')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Đơn giá</label>
                            <input type="text" class="form-control" name="price"
                                   value="{{$khachHangKhungGio->khunggio->price}}" readonly
                                   maxlength="255">
                            @if ($errors->has('price'))
                                <p class="text-danger">{{$errors->first('price')}}</p>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mr-2 add btn-sm a-font-size-13"
                                title="Lưu mới">
                            <i class="fa fa-save"></i> Đặt sân
                        </button>
                        <a href="{{route('khach-hang-khung-gio.index')}}" class="btn btn-secondary btn-sm a-font-size-13"
                           title="Quay lại">
                            <i class="fa fa-arrow-left"></i> Quay lại
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/datsan.js')}}"></script>
@endsection
