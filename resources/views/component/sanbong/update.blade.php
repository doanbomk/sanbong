@extends('app')
@section('head.title')
    Chỉnh sửa sân bóng
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-12 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Chỉnh sửa sân bóng</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-block">
                <form class="forms-sample" method="POST" action="{{route('san-bong.update', $pitch->id)}}">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="id" value="{{ $pitch->id }}" hidden>
                            <label class="col-form-label">Mã sân bóng<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="code" value="{{ $pitch->code }}" autocomplete="off" maxlength="50" readonly>
                            @if ($errors->has('code'))
                                <p class="text-danger">{{$errors->first('code')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Tên sân bóng<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ $pitch->name }}" autocomplete="off" maxlength="255">
                            @if ($errors->has('name'))
                                <p class="text-danger">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        {{--<div class="form-group col-md-3">--}}
                            {{--<label class="col-form-label">Số điện thoại</label>--}}
                            {{--<input type="text" class="form-control" name="phone" value="{{$user->phone}}" maxlength="20">--}}
                            {{--@if ($errors->has('phone'))--}}
                                {{--<p class="text-danger">{{$errors->first('phone')}}</p>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-3">--}}
                            {{--<label class="col-form-label">Email</label>--}}
                            {{--<input type="text" class="form-control" name="email" value="{{ $user->email }}"--}}
                                   {{--autocomplete="off" maxlength="50">--}}
                            {{--@if ($errors->has('email'))--}}
                                {{--<p class="text-danger">{{$errors->first('email')}}</p>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    </div>

                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mr-2 add btn-sm a-font-size-13" title="Cập nhật">
                            <i class="fa fa-save"></i> Cập nhật
                        </button>
                        <a href="{{route('san-bong.index')}}" class="btn btn-secondary btn-sm a-font-size-13"
                           title="Quay lại">
                            <i class="fa fa-arrow-left"></i> Quay lại
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/sanbong.js')}}"></script>
@endsection
