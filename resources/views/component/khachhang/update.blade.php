@extends('app')
@section('head.title')
    Chỉnh sửa khách hàng
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-12 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Chỉnh sửa khách hàng</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-block">
                <form class="forms-sample" method="POST" action="{{route('khach-hang.update', $customer->id)}}">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="id" value="{{ $customer->id }}" hidden>
                            <label class="col-form-label">Mã khách hàng<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="code" value="{{ $customer->code }}" autocomplete="off" maxlength="50" readonly>
                            @if ($errors->has('code'))
                                <p class="text-danger">{{$errors->first('code')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Họ tên<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ $customer->name }}" autocomplete="off" maxlength="255">
                            @if ($errors->has('name'))
                                <p class="text-danger">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                        <label class="col-form-label">Số điện thoại <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="phone" value="{{$customer->phone}}" maxlength="20">
                        @if ($errors->has('phone'))
                        <p class="text-danger">{{$errors->first('phone')}}</p>
                        @endif
                        </div>
                        <div class="form-group col-md-3">
                        <label class="col-form-label">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ $customer->email }}"
                        autocomplete="off" maxlength="50">
                        @if ($errors->has('email'))
                        <p class="text-danger">{{$errors->first('email')}}</p>
                        @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Giới tính</label>
                            <div class="form-radio">
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="gender" value="1" @if($customer->gender == 1) checked @endif>
                                        <i class="helper"></i>Nam
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="gender" value="2" @if($customer->gender == 2) checked @endif>
                                        <i class="helper"></i>Nữ
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="gender" value="3" @if($customer->gender == 3) checked @endif>
                                        <i class="helper"></i>Khác
                                    </label>
                                </div>
                            </div>
                            @if ($errors->has('gender'))
                                <p class="text-danger">{{$errors->first('gender')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Địa chỉ</label>
                            <input type="text" class="form-control" name="address" value="{{$customer->address}}"
                                   maxlength="255">
                            @if ($errors->has('address'))
                                <p class="text-danger">{{$errors->first('address')}}</p>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mr-2 add btn-sm a-font-size-13" title="Cập nhật">
                            <i class="fa fa-save"></i> Cập nhật
                        </button>
                        <a href="{{route('khach-hang.index')}}" class="btn btn-secondary btn-sm a-font-size-13"
                           title="Quay lại">
                            <i class="fa fa-arrow-left"></i> Quay lại
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/khachhang.js')}}"></script>
@endsection

