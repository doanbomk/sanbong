@extends('app')
@section('head.title')
    Chỉnh sửa khung giờ
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-12 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Chỉnh sửa khung giờ</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-block">
                <form class="forms-sample" method="POST" action="{{route('khung-gio.update', $timeframe->id)}}">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="id" value={{ $timeframe->id }} hidden>
                            <label class="col-form-label">Mã khung giờ<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="code" value="{{ $timeframe->code }}"
                                   autocomplete="off" maxlength="50" readonly>
                            @if ($errors->has('code'))
                                <p class="text-danger">{{$errors->first('code')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Tên khung giờ<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ $timeframe->name }}"
                                   autocomplete="off" maxlength="255">
                            @if ($errors->has('name'))
                                <p class="text-danger">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Thời gian bắt đầu <span class="text-danger">*</span></label>
                            <input type="time" class="form-control" name="start_time" value="{{ $timeframe->start_time}}">
                            @if ($errors->has('start_time'))
                                <p class="text-danger">{{$errors->first('start_time')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Thời gian kết thúc <span class="text-danger">*</span></label>
                            <input type="time" class="form-control" name="end_time" value="{{ $timeframe->end_time }}"
                                   autocomplete="off" maxlength="50">
                            @if ($errors->has('end_time'))
                                <p class="text-danger">{{$errors->first('end_time')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Giá tiền<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="price" value="{{ $timeframe->price }}"
                                   autocomplete="off" maxlength="50">
                            @if ($errors->has('price'))
                                <p class="text-danger">{{$errors->first('price')}}</p>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mr-2 add btn-sm a-font-size-13" title="Cập nhật">
                            <i class="fa fa-save"></i> Cập nhật
                        </button>
                        <a href="{{route('khung-gio.index')}}" class="btn btn-secondary btn-sm a-font-size-13"
                           title="Quay lại">
                            <i class="fa fa-arrow-left"></i> Quay lại
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/khunggio.js')}}"></script>
@endsection

