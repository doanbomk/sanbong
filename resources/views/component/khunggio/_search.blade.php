<div>
    <form id="form-search" method="get" action="{{route('khung-gio.index')}}">
        <div class="form-row">
            <div class="col-md-3">
                <input id="search_input" type="text" class="form-control" name="search" placeholder="Mã hoặc tên khung giờ" value="{{request('search')}}">
            </div>
            <div class="col-md-1">
                <button class="btn btn-inverse btn-sm" type="submit" title="Tìm kiếm">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>
</div>

