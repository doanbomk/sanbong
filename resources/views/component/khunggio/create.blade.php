@extends('app')
@section('head.title')
    Thêm mới khung giờ
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-12 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Thêm mới khung giờ</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-block">
                <form class="forms-sample" method="post" action="{{route('khung-gio.store')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Mã khung giờ<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="code" value="{{ old('code') }}"
                                   autocomplete="off" maxlength="50">
                            @if ($errors->has('code'))
                                <p class="text-danger">{{$errors->first('code')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Tên khung giờ<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}"
                                   autocomplete="off" maxlength="255">
                            @if ($errors->has('name'))
                                <p class="text-danger">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Thời gian bắt đầu <span class="text-danger">*</span></label>
                            <input type="time" class="form-control" name="start_time"  value="{{ old('start_time')}}">
                            @if ($errors->has('start_time'))
                                <p class="text-danger">{{$errors->first('start_time')}}</p>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Thời gian kết thúc <span class="text-danger">*</span></label>
                            <input type="time" class="form-control" name="end_time" value="{{ old('end_time') }}"
                                   autocomplete="off" maxlength="50">
                            @if ($errors->has('end_time'))
                                <p class="text-danger">{{$errors->first('end_time')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label">Giá tiền<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="price" value="{{ old('price') }}"
                                   autocomplete="off" maxlength="50">
                            @if ($errors->has('price'))
                                <p class="text-danger">{{$errors->first('price')}}</p>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mr-2 add btn-sm a-font-size-13" title="Lưu mới">
                            <i class="fa fa-save"></i> Lưu mới
                        </button>
                        <a href="{{route('khung-gio.index')}}" class="btn btn-secondary btn-sm a-font-size-13"
                           title="Quay lại">
                            <i class="fa fa-arrow-left"></i> Quay lại
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/khung-gio.js')}}"></script>
@endsection
