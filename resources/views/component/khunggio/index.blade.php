@extends('app')
@section('head.title')
    Quản lý khung giờ
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-8 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Quản lý khung giờ</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="float-right p-r-10">
                    <a class="btn btn-primary btn-sm color-white" title="Thêm mới" href="{{route('khung-gio.create')}}">
                        <i class="fa fa-plus"></i> Thêm mới
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-header p-b-15 p-t-10 header-form-search">
                @include('component.khunggio._search')
            </div>
            <div class="card-body p-t-0">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-custom">
                        <thead class="t-head-inverse">
                        <tr>
                            <th>STT</th>
                            <th>Mã</th>
                            <th>Tên khung giờ</th>
                            <th>Thời gian bắt đầu</th>
                            <th>Thời gian kết thúc</th>
                            <th>Giá tiền</th>
                            <th>Tác vụ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $index = $timeframe->perpage() * ($timeframe->currentPage() - 1);
                        @endphp
                        @foreach($timeframe as $key => $value)
                            <tr>
                                <td class="text-center">{{$key + 1 + $index}}</td>
                                <td>{{$value->code}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->start_time}}</td>
                                <td>{{$value->end_time}}</td>
                                <td>{{$value->price}}</td>
                                <td class="text-center">
                                    <a class="p-l-5" href="{{route('khung-gio.edit', $value->id)}}" title="Chỉnh sửa">
                                        <i class="fa fa-edit fa-lg"></i>
                                    </a>

                                    <a class="del-record color-red p-l-5" data-id="{{$value->id}}"
                                       href="javascript:void(0)">
                                        <i class="fa fa-trash fa-lg" title="Xóa"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        @include('component.pagination', ['column' => 7, 'datas' => $timeframe])
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        @include('component.flash-message')
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/khunggio.js')}}"></script>
@endsection

