@extends('app')
@section('head.title')
    Quản lý đặt sân
@endsection
@section('content')
    <div class="page-header m-t-150 page-header-index">
        <div class="row">
            <div class="col-lg-8 p-t-5">
                <div class="page-header-title p-l-10">
                    <div class="d-inline">
                        <h4>Quản lý đặt sân</h4>
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-4">--}}
                {{--<div class="float-right p-r-10">--}}
                    {{--<a class="btn btn-primary btn-sm color-white" title="Thêm mới"--}}
                       {{--href="{{route('khach-hang-khung-gio.create')}}">--}}
                        {{--<i class="fa fa-plus"></i> Thêm mới--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="page-body">
        <div class="card card-index">
            <div class="card-header p-b-15 p-t-10 header-form-search">
                @include('component.khachhang-khunggio._search',$customer)

            </div>
            <div class="card-body p-t-0">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-custom">
                        <thead class="t-head-inverse">
                        <tr>
                            <th>STT</th>
                            <th>Tên sân</th>
                            <th>Tên khung giờ</th>
                            <th>Tên khách hàng</th>
                            <th>Ngày</th>
                            <th>Trạng thái</th>
                            <th>TT thanh toán</th>
                            <th>Thanh toán</th>
                            <th>Tác vụ</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php
                            $index = $calendar->perpage() * ($calendar->currentPage() - 1);
                        @endphp
                        @foreach($calendar as $key => $value)
                            <tr>
                                <td class="text-center">{{$key + 1 + $index}}</td>
                                <td>{{$value->san->name ?? ''}}</td>
                                <td>{{$value->khunggio->name ?? ''}}</td>
                                <td>{{$value->customer->name ?? ''}}</td>
                                <td>{{$value->active_date}}</td>
                                <td class="text-center">
                                    @if ($value->status == 1)
                                        <label class="badge badge-success">Đã đặt</label>
                                    @else
                                        <label class="badge badge-danger">Chưa đặt</label>
                                    @endif
                                </td>

                                <td class="text-center">
                                    @if ($value->status_thanh_toan == 1)
                                        <label class="badge badge-info">Đã thanh toán</label>
                                    @else
                                        <label class="badge badge-warning">Chưa thanh toán</label>
                                    @endif
                                <td>{{$value->thanh_toan ? $value->thanh_toan:'0'}}</td>
                                <td class="text-center">
                                    <a class="p-l-5" href="{{route('khach-hang-khung-gio.edit', $value->id)}}"
                                       title="Đặt sân">
                                        <i class="fa fa-calendar-check-o fa-lg"></i>
                                    </a>
                                    <a class="color-red p-l-5"
                                       href="{{route('khach-hang-khung-gio.payment', $value->id)}}" title="Thanh toán">
                                        <i class="fa fa-money fa-lg" ></i>
                                    </a>
                                    <a class=" color-red p-l-5"
                                       href="{{route('khach-hang-khung-gio.show', $value->id)}}" title="Xem chi tiết">
                                        <i class="fa fa-info-circle fa-lg" ></i>
                                    </a>
                                    {{--<i class="fa fa-money"></i>--}}
                                    <a class="cancel color-red p-l-5" data-id="{{$value->id}}" style="color: red"
                                       href="javascript:void(0)" title="Hủy đặt sân">
                                        <i class="fa fa-times-circle-o fa-lg" ></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        @include('component.pagination', ['column' => 9, 'datas' => $calendar])
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        @include('component.flash-message')
    </div>
@endsection
@section('script')
    <script type="module" src="{{asset('js/modules/datsan.js')}}"></script>
@endsection


