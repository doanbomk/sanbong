<div>
    <form id="form-search" method="get" action="{{route('khach-hang-khung-gio.index')}}">

        <div class="form-row">
            <div class="col-md-3">
                <label class="col-form-label">Trạng thái sân</label>
                <select style="height: 34px" class="form-control form-control-sm js-select2-no-search"
                        name="status">
                    <option value="">--- Trạng thái sân ---</option>
                    <option @if(request('status') == 1) selected
                            @endif value="1">Đã đặt
                    </option>
                    <option @if(request('status') == 2) selected
                            @endif value="2">Chưa đặt
                    </option>
                </select>
            </div>
            <div class="col-md-3">
                <label class="col-form-label">Tên khách hàng</label>
                <select style="height: 34px" class="form-control form-control-sm js-select2-no-search"
                        name="customer">
                    <option value="">--- Tên khách hàng ---</option>
                    @foreach($customer as $key => $value)
                        <option @if(request('customer') == $key+1) selected
                                @endif value="{{$value->id}}">{{$value['name']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <label class="col-form-label">Ngày đá</label>
                <input style="height: 34px" id="search_input" type="date" class="form-control"
                       name="active_date" value="{{request('active_date')}}">
            </div>

            <div class="col-md-3">
                <label class="col-form-label">Khung giờ</label>
                <select style="height: 34px" class="form-control form-control-sm js-select2-no-search"
                        id="search_service"
                        name="timeframe">
                    <option value="">--- Khung giờ ---</option>
                    @foreach($time as $key => $value)
                        <option @if(request('timeframe') == $key+1) selected
                                @endif value="{{$value->id}}">{{$value['name']}}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <br/>
        <div class="row">
            <div class="form-group col-md-1">
                <button class="btn btn-inverse btn-sm" type="submit" title="Tìm kiếm">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>
</div>
