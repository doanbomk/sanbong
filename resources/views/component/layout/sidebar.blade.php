<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Quản lý</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('user.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                    <span class="pcoded-mtext">Quản lý tài khoản</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('khach-hang.index')}}">
                    <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                    <span class="pcoded-mtext">Quản lý khách hàng</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('san-bong.index')}}">
                    <span class="pcoded-micon"><i class="fa fa-soccer-ball-o"></i></span>
                    <span class="pcoded-mtext">Quản lý sân bóng</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('khung-gio.index')}}">
                    <span class="pcoded-micon"><i class="fa fa-calendar-o"></i></span>
                    <span class="pcoded-mtext">Quản lý khung giờ</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('khach-hang-khung-gio.index')}}">
                    <span class="pcoded-micon"><i class="fa fa-gavel"></i></span>
                    <span class="pcoded-mtext">Quản lý yêu cầu đặt sân</span>
                </a>
            </li>
            {{--<li class="">--}}
                {{--<a href="navbar-light.htm">--}}
                    {{--<span class="pcoded-micon"><i class="feather icon-file-minus"></i></span>--}}
                    {{--<span class="pcoded-mtext">Quản lý hóa đơn</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        </ul>
    </div>
</nav>
