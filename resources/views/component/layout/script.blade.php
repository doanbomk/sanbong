
<script type="text/javascript" src="{{asset('template/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/bootstrap.min.js')}}"></script>
<!--datepicker-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="{{asset('template/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/datepicker.vi.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{asset('template/js/jquery.slimscroll.js')}}"></script>

<script src="{{asset('template/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/SmoothScroll.js')}}"></script>
<script src="{{asset('template/js/pcoded.min.js')}}"></script>
<!-- custom js -->
<script src="{{asset('template/js/vartical-layout.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/script.min.js')}}"></script>

