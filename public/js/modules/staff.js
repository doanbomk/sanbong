import Resource from './resource/app.js';
const staffResource = new Resource();
var Config = (function () {
    staffResource.get_csrf_ajax();
    var handleConfirmation = function () {
        staffResource.destroy('user/del_user');
        staffResource.datepicker('.datepicker-single');
        staffResource.change_selected_page('.number_raw');

    };

    return {
        init: function () {
            handleConfirmation()
        }
    }
})();

jQuery(document).ready(function () {
    Config.init();
});

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}

$(".number_raw").change(function () {
    var raw = $('.number_raw').val();
    window.location = updateQueryStringParameter(window.location.href, 'raw', raw);
});
