const http = 'http://localhost:88/kitchen/public/';
class Resource {
    get_csrf_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name=csrf-token]').attr('content')
            }
        });
    }

    swal_1500(message, status) {
        swal(message, {
            buttons: false,
            timer: 1500,
            icon: status
        });
    }

    destroy(uriName) {
        var self = this;
        $('.del-record').on('click', function () {
            var id = $(this).data('id');
            if(id && typeof id !== 'undefined'){
                swal("Bạn có chắc chắn muốn xóa bản ghi này?", {
                    buttons: ["Thoát", "Đồng ý"]
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: http + uriName + '/' + id,
                                type: 'DELETE',
                                success: function (response) {
                                    self.swal_1500(response.message, response.status);
                                    location.reload();
                                },
                                error: function () {
                                    self.swal_1500('Có lỗi xảy ra', 'error')
                                }
                            })
                        }
                    });
            }

        });
    };


    cancel(uriName) {
        var self = this;
        $('.cancel').on('click', function () {
            var id = $(this).data('id');
            if(id && typeof id !== 'undefined'){
                swal("Bạn có chắc chắn muốn hủy đặt sân này?", {
                    buttons: ["Thoát", "Đồng ý"]
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: http + uriName + '/' + id,
                                type: 'DELETE',
                                success: function (response) {
                                    self.swal_1500(response.message, response.status);
                                    location.reload();
                                },
                                error: function () {
                                    self.swal_1500('Có lỗi xảy ra', 'error')
                                }
                            })
                        }
                    });
            }

        });
    };

    datepicker(el) {
        $(el).datepicker({
            language: 'vi',
            autoclose: true,
            autoOpen: false,
        });
    };

    change_selected_page(el)
    {
        $(el).on('change', function () {
            var paged = $(this).val();
            var url = '';
            console.log(paged);
        })
    }
}

export {Resource as default}
