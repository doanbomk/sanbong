<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KhachHang extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'email', 'gender', 'address', 'phone',
    ];

    protected $table = 'khach_hang';
    protected $perPage = 15;
}
