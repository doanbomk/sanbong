<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KhungGio extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','code','start_time','end_time','price','created_at','updated_by','updated_at','created_by'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'khung_gio';
    protected $perPage = 15;
}
