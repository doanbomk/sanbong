<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KhachHangKhungGio extends Model
{
//    use SoftDeletess;
    protected $fillable = [
        'id_khach_hang','id_khung_gio','active_date','id_san','status','id_user','updated_at',
        'thanh_toan','status_thanh_toan','deleted_at','type'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $table = 'khachhang_khunggio';
    protected $perPage = 15;

    public function customer()
    {
        return $this->belongsTo('App\KhachHang', 'id_khach_hang', 'id');
    }

    public function san()
    {
        return $this->belongsTo('App\SanBong', 'id_san', 'id');
    }

    public function khunggio()
    {
        return $this->belongsTo('App\KhungGio', 'id_khung_gio', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
}
