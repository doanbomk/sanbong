<?php

namespace App\Http\Controllers;

use App\Http\Requests\SanBongRequest;
use App\SanBong;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SanBongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pitch = SanBong::where(function ($q) use ($request) {
            if ($request->search) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
            }
        })->paginate($request->raw);
        return view('component.sanbong.index', compact('pitch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('component.sanbong.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SanBongRequest $request)
    {
        try {
            SanBong::create([
                'code' => $request->code,
                'name' => $request->name,
            ]);
            return redirect()->route('san-bong.index')->with(['status' => 'success', 'message' => 'Thêm mới thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Thêm mới sân bóng không thành công']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SanBong $sanBong
     * @return \Illuminate\Http\Response
     */
    public function show(SanBong $sanBong)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SanBong $sanBong
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pitch = SanBong::findOrFail($id);
        return view('component.sanbong.update', compact('pitch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\SanBong $sanBong
     * @return \Illuminate\Http\Response
     */
    public function update(SanBongRequest $request)
    {
        try {
            $data = [
                'name' => $request->name,
            ];

            SanBong::find($request->id)->update($data);
            return redirect()->route('san-bong.index')->with(['status' => 'success', 'message' => 'Chỉnh sửa thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Chỉnh sửa tài khoản không thành công']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SanBong $sanBong
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            SanBong::destroy($id);
            return response()->json([
                'status' => 'success',
                'message' => 'Xóa bản ghi thành công'
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'error',
                'message' => 'Có lỗi xảy ra'
            ], 500);
        }
    }
}
