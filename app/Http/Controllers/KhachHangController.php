<?php

namespace App\Http\Controllers;

use App\Http\Requests\KhachHangRequest;
use App\KhachHang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;

class KhachHangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customer = KhachHang::where(function ($q) use ($request) {
            if ($request->search) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
            }
        })->paginate($request->raw);
        return view('component.khachhang.index', compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('component.khachhang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(KhachHangRequest $request)
    {
        try {
            $code = generate_code_customer();
            KhachHang::create([
                'code' => $code,
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'gender' => $request->gender,
            ]);
            return redirect()->route('khach-hang.index')->with(['status' => 'success', 'message' => 'Thêm mới thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Thêm mới khách hàng không thành công']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KhachHang $khachHang
     * @return \Illuminate\Http\Response
     */
    public function show(KhachHang $khachHang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KhachHang $khachHang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = KhachHang::findOrFail($id);
        return view('component.khachhang.update', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\KhachHang $khachHang
     * @return \Illuminate\Http\Response
     */
    public function update(KhachHangRequest $request)
    {
        try {
            $data = [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'gender' => $request->gender,
            ];

            KhachHang::find($request->id)->update($data);
            return redirect()->route('khach-hang.index')->with(['status' => 'success', 'message' => 'Chỉnh sửa thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Chỉnh sửa khách hàng không thành công']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KhachHang $khachHang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            KhachHang::destroy($id);
            return response()->json([
                'status' => 'success',
                'message' => 'Xóa bản ghi thành công'
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'error',
                'message' => 'Có lỗi xảy ra'
            ], 500);
        }
    }

    public function generate_code_customer()
    {
        $customer = KhachHang::latest('id')->first()->code;
        $sub = substr($customer, 2) + 1;
        $code = substr($customer, 0, -1) . $sub;
        return $code;
    }
}
