<?php

namespace App\Http\Controllers;

use App\Http\Requests\KhungGioRequest;
use App\KhachHang;
use App\KhachHangKhungGio;
use App\KhungGio;
use App\SanBong;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class KhungGioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $timeframe = KhungGio::where(function ($q) use ($request) {
            if ($request->search) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
            }
        })->paginate($request->raw);
        return view('component.khunggio.index', compact('timeframe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            return view('component.khunggio.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(KhungGioRequest $request)
    {
        try {
            KhungGio::create([
                'code' => $request->code,
                'name' => $request->name,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'price' => $request->price,
            ]);

            return redirect()->route('khung-gio.index')->with(['status' => 'success', 'message' => 'Thêm mới thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Thêm mới khung giờ không thành công']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KhungGio $khungGio
     * @return \Illuminate\Http\Response
     */
    public function show(KhungGio $khungGio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KhungGio $khungGio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $timeframe = KhungGio::findOrFail($id);
        return view('component.khunggio.update', compact('timeframe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\KhungGio $khungGio
     * @return \Illuminate\Http\Response
     */
    public function update(KhungGioRequest $request)
    {
        try {
            $data = [
                'name' => $request->name,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'price' => $request->price,
            ];


            KhungGio::find($request->id)->update($data);
            return redirect()->route('khung-gio.index')->with(['status' => 'success', 'message' => 'Chỉnh sửa thành công']);
        } catch (\Exception $ex) {
            dd($ex);
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Chỉnh sửa khung giờ không thành công']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KhungGio $khungGio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            KhungGio::find($id)->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'Xóa bản ghi thành công'
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'error',
                'message' => 'Có lỗi xảy ra'
            ], 500);
        }
    }
}
