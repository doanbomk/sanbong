<?php

namespace App\Http\Controllers;

use App\Http\Requests\KhachHangKhungGioRequest;
use App\KhachHang;
use App\KhachHangKhungGio;
use App\KhungGio;
use App\SanBong;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class KhachHangKhungGioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $time = KhungGio::get();
        $customer = KhachHang::get();
        $calendar = KhachHangKhungGio::with('customer', 'san', 'user', 'khunggio')->where(function ($q) use ($request) {
            if ($request->search) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
            }
            if ($request->timeframe) {
                $q->where('id_khung_gio', $request->timeframe);
            }
            if ($request->timeframe) {
                $q->where('id_khach_hang', $request->customer);
            }
            if ($request->active_date) {
                $q->where('active_date', $request->active_date);
            }
            if ($request->status) {
                $q->where('status', $request->status);
            }
        })->paginate($request->raw);
        return view('component.khachhang-khunggio.index', compact('calendar', 'time', 'customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $time = KhungGio::get();
        $customer = KhachHang::get();
        return view('component.dat-san.index', compact('time', 'customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KhachHangKhungGio $khachHangKhungGio
     * @return \Illuminate\Http\Response
     */
    public function show(KhachHangKhungGio $khachHangKhungGio)
    {
        $time = KhungGio::get();
        $customer = KhachHang::get();
        $san = SanBong::get();
        return view('component.khachhang-khunggio.view', compact('time', 'customer', 'khachHangKhungGio', 'san'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KhachHangKhungGio $khachHangKhungGio
     * @return \Illuminate\Http\Response
     */
    public function edit(KhachHangKhungGio $khachHangKhungGio)
    {
        $time = KhungGio::get();
        $customer = KhachHang::get();
        $san = SanBong::get();
        return view('component.dat-san.index', compact('time', 'customer', 'khachHangKhungGio', 'san'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\KhachHangKhungGio $khachHangKhungGio
     * @return \Illuminate\Http\Response
     */
    public function update(KhachHangKhungGioRequest $request, KhachHangKhungGio $khachHangKhungGio)
    {
        try {
            $code = generate_code_customer();
            $customer = KhachHang::updateOrCreate(
                [
                    'phone' => request('phone')
                ],
                [
                    'code' => $code,
                    'name' => $request->name,
                    'created_by' => Auth::user()->id,
                    'email' => $request->email,
                    'address' => $request->address,
                    'gender' => $request->gender,
                ]
            );
            if (request('type') == 1) {
                KhachHangKhungGio::where('id_khung_gio', $khachHangKhungGio->id_khung_gio)
                    ->where('id_san', $khachHangKhungGio->id_san)
                    ->whereBetween('active_date', [Carbon::today(), '2020-12-31'])->update([
                        'id_khach_hang' => $customer->id,
                        'status' => 1,
                        'type' => request('type')
                    ]);

            } else {
                $khachHangKhungGio->update(
                    [
                        'id_khach_hang' => $customer->id,
                        'status' => 1,
                        'type' => request('type')
                    ]
                );
            }
            return redirect()->route('khach-hang-khung-gio.index')->with(['status' => 'success', 'message' => 'Đặt sân thành công']);

        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Đặt sân không thành công']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KhachHangKhungGio $khachHangKhungGio
     * @return \Illuminate\Http\Response
     */
    public function destroy(KhachHangKhungGio $khachHangKhungGio)
    {
        try {
            $khachHangKhungGio->update([
                'status' => 2
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Xóa hủy đặt sân thành công'
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'error',
                'message' => 'Có lỗi xảy ra'
            ], 500);
        }
    }

    public function cancel_all(Request $request)
    {
        try {
            $khachHangKhungGio = KhachHangKhungGio::findOrFail($request->id);
            $data = KhachHangKhungGio::where('id_khung_gio', $khachHangKhungGio->id_khung_gio)
                ->where('id_san', $khachHangKhungGio->id_san)
                ->whereBetween('active_date', [Carbon::today(), '2020-12-31'])
                ->update([
                    'id_khach_hang' => null,
                    'status' => 2,
                    'status_thanh_toan' => 2,
                    'type' => null
                ]);
//            dd($data,$khachHangKhungGio);
            return redirect()->route('khach-hang-khung-gio.index')->with(['status' => 'success', 'message' => 'Hủy hợp đồng thành công']);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'error',
                'message' => 'Có lỗi xảy ra'
            ], 500);
        }
    }

    public function payment($id) {
        $khachHangKhungGio = KhachHangKhungGio::findOrFail($id);
//        dd($khachHangKhungGio);
        $time = KhungGio::get();
        $customer = KhachHang::get();
        $san = SanBong::get();
        return view('component.dat-san.payment', compact('time','customer','san','khachHangKhungGio'));
    }

    public function confirm_payment(Request $request) {

        if($request->price >= $request->payment) {
            $payment = 1;
        } else {
            $payment = 2;
        }
        KhachHangKhungGio::find($request->id)->update([
            'status_thanh_toan'=>$payment,
        ]);
        return redirect()->route('khach-hang-khung-gio.index')->with(['status' => 'success', 'message' => 'Thanh toán thành công']);
    }
}
