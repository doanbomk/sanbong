<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where(function ($q) use ($request) {
            if ($request->username) {
                $q->where('code', 'like', '%' . $request->username . '%')->orWhere('username', 'like', '%' . $request->username . '%');
            }
        })->paginate($request->raw);
        return view('component.user.index', compact('users'));
    }

    public function create()
    {
        return view('component.user.create');
    }

    public function store(UserRequest $request)
    {
        try {
            User::create([
                'code' => $request->code,
                'name' => $request->name,
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'phone' => $request->phone,
                'address' => $request->address,
                'email' => $request->email,
            ]);
            return redirect()->route('user.index')->with(['status' => 'success', 'message' => 'Thêm mới thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Thêm mới tài khoản không thành công']);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('component.user.update', compact('user'));
    }

    public function update(UserRequest $request)
    {
        try {
            $data = [
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address,
                'email' => $request->email,
            ];
            if (!empty($request->password)) {
                $data = [
                    'name' => $request->name,
                    'password' => bcrypt($request->password),
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'email' => $request->email,
                ];
            }

            User::find($request->id)->update($data);
            return redirect()->route('user.index')->with(['status' => 'success', 'message' => 'Chỉnh sửa thành công']);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Chỉnh sửa tài khoản không thành công']);
        }
    }

    public function destroy($id)
    {
        try {
            User::destroy($id);
            return response()->json([
                'status' => 'success',
                'message' => 'Xóa bản ghi thành công'
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'error',
                'message' => 'Có lỗi xảy ra'
            ], 500);
        }
    }
}
