<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KhungGioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255,',
            'code' => 'required|max:255|unique:khung_gio,code,' . $this->id,
//            'start_time' => 'required|date_format:H:i:s',
//            'end_time' => 'required|date_format:H:i:s',
            'price' => 'required|max:14',
//            'gender' => 'max:14',
        ];
        if ($this->id) {
            $rules['start_time'] = 'required|date_format:H:i:s';
            $rules['end_time'] = 'required|date_format:H:i:s';
        } else {
            $rules['start_time'] = 'required|date_format:H:i';
            $rules['end_time'] = 'required|date_format:H:i';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên khung giờ không được bỏ trống.',
            'code.required' => 'Mã khung giờ không được bỏ trống.',
            'start_time.required' => 'thời gian bắt dầu không được bỏ trống.',
            'end_time.required' => 'thời gian kết thúc không được bỏ trống.',
            'price.required' => 'Đơn giá không được bỏ trống',
            'name.max' => 'Tên người dùng tối đa 255 kí tự.',
            'code.max' => 'Mã người dùng tối đa 255 kí tự.',
            'start_time.date_format' => 'thời gian bắt đầu không đúng',
            'end_time.date_format' => 'thời gian kết thúc không đúng',
            'code.unique' => 'Mã này đã được sử dụng',
        ];
    }
}
