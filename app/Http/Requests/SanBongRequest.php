<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SanBongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255,',
            'code' => 'required|max:255|unique:users,code,' . $this->id,
            ];
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên sân bóng không được bỏ trống.',
            'name.max' => 'Tên sân bóng tối đa 255 kí tự.',
            'code.required' => 'Mã sân bóng không được bỏ trống.',
            'code.max' => 'Mã sân bóng tối đa 255 kí tự.',
            'code.unique' => 'Mã này đã được sử dụng',
        ];
    }
}
