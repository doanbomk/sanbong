<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'username' => 'required|max:255|unique:users,username,' . $this->id,
            'name' => 'required|max:255,',
            'code' => 'required|max:255|unique:users,code,' . $this->id,
//            'email' => 'max:255|email',
            'address' => 'max:255',
            'phone' => 'required|max:14|unique:users,phone,' . $this->id,
        ];
        if (!$this->id) {
            $rules['password'] = 'required';
            $rules['confirm_password'] = 'required|same:password';
        }
        if ($this->email) {
            $rules['email'] = 'max:255|email';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'username.required' => 'Tên đăng nhập không được bỏ trống.',
            'name.required' => 'Tên người dùng không được bỏ trống.',
            'code.required' => 'Mã người dùng không được bỏ trống.',
            'phone.required' => 'Số điện thoại không được bỏ trống.',
//            'email.required' => 'Email không được bỏ trống.',
            'password.required' => 'Mật khẩu không được bỏ trống.',
            'confirm_password.required' => 'Xác nhận mật khẩu không được bỏ trống.',
            'username.max' => 'Tên đăng nhập tối đa 255 kí tự.',
            'name.max' => 'Tên người dùng tối đa 255 kí tự.',
            'code.max' => 'Mã người dùng tối đa 255 kí tự.',
            'email.max' => 'Email tối đa 255 kí tự.',
            'confirm_password.same' => 'Mật khẩu chưa trùng khớp.',
//            'confirm_password.required' => 'Mật khẩu nhập lại không được bỏ trống.',
            'email.email' => 'Email chưa đúng định dạng.',
            'code.unique' => 'Mã này đã được sử dụng',
            'username.unique' => 'Tên đăng nhập này đã được sử dụng',
//            'phone.numeric'=>'Số điện thoại phải là số',
//            'phone.min'=>'Số điện thoại không được nhỏ hơn 10 chữ số',
            'phone.max' => 'Số điện thoại không được lớn hơn 14 chữ số',
            'phone.unique' => 'Số điện thoại này đã được sử dụng',
        ];
    }
}
