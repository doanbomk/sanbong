<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KhachHangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255,',
//            'code' => 'required|max:255|unique:khach_hang,code,' . $this->id,
            'address' => 'max:255',
            'phone' => 'required|max:14|unique:khach_hang,phone,' . $this->id,
//            'gender' => 'max:14',
        ];
        if ($this->email) {
            $rules['email'] = 'max:255|email';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên khách hàng không được bỏ trống.',
            'code.required' => 'Mã khách hàng không được bỏ trống.',
            'name.max' => 'Tên khách hàng tối đa 255 kí tự.',
            'code.max' => 'Mã khách hàng tối đa 255 kí tự.',
            'email.max' => 'Email tối đa 255 kí tự.',
            'address.max' => 'Email tối đa 255 kí tự.',
            'email.email' => 'Email chưa đúng định dạng.',
            'code.unique' => 'Mã này đã được sử dụng',
            'phone.unique' => 'Số điện thoại này này đã được sử dụng',
//            'username.unique' => 'Tên khách hàng này đã được sử dụng',
            'phone.max' => 'Số điện thoại không được lớn hơn 14 chữ số',
            'phone.required' => 'Số điện thoại không được để trống',
        ];
    }
}
