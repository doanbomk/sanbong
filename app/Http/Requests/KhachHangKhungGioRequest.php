<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KhachHangKhungGioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255,',
//            'code' => 'required|max:255|unique:khachhang_khunggio,code,' . $this->id,
            'email' => 'max:255|email',
            'address' => 'max:255',
            'phone' => 'required|max:14',
//            'gender' => 'max:14',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên người dùng không được bỏ trống.',
            'code.required' => 'Mã người dùng không được bỏ trống.',
            'name.max' => 'Tên người dùng tối đa 255 kí tự.',
            'code.max' => 'Mã người dùng tối đa 255 kí tự.',
            'email.max' => 'Email tối đa 255 kí tự.',
            'email.email' => 'Email chưa đúng định dạng.',
            'code.unique' => 'Mã này đã được sử dụng',
            'username.unique' => 'Tên đăng nhập này đã được sử dụng',
            'phone.max' => 'Số điện thoại không được lớn hơn 14 chữ số',
        ];
    }
}
