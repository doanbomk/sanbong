<?php
use Illuminate\Support\Facades\DB;
use App\KhachHang;

if (!function_exists('generate_staff')) {
    function generate_staff()
    {
        try {
            $staff = DB::table('staff')->selectRaw('MAX(SUBSTR(code,3)) as code')->first();
            $sttCode = !empty($staff) ? (int)$staff->code : 0;
            $code = 'NV' . sprintf("%04s", $sttCode + 1);

            return $code;
        } catch (Exception $ex) {
            abort(500, 'Có lỗi xảy ra');
        }
    }
}

if(!function_exists('list_number_page')){
    function list_number_page()
    {
        return ['0' => '15', '1' => '20', '2' => '30', '3' => '40'];
    }
}

if(!function_exists('generate_code_customer')){
    function generate_code_customer()
    {
        try {
            $customer = DB::table('khach_hang')->selectRaw('MAX(SUBSTR(code,3)) as code')->first();
            $sttCode = !empty($customer) ? (int)$customer->code : 0;
            $code = 'KH' . sprintf("%04s", $sttCode + 1);

            return $code;
        } catch (Exception $ex) {
            abort(500, 'Có lỗi xảy ra');
        }
    }
}
