<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\SanBong;
use App\KhachHangKhungGio;
use App\KhungGio;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//clear cache
Route::get('clearcache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});

//nhan vien route
Route::group(['middleware' => ['auth']], function () {
    Route::resources([
        'staff' => StaffController::class,
    ]);

    Route::get('/', function () {
        return redirect(route('staff.index'));
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('danh-sach', 'UserController@index')->name('user.index');
        Route::get('them-moi', 'UserController@create')->name('user.create');
        Route::post('them-moi', 'UserController@store')->name('user.confirm.create');
        Route::get('cap-nhat/{id}', 'UserController@edit')->name('user.edit');
        Route::post('cap-nhat', 'UserController@update')->name('user.confirm.edit');
        Route::get('tim-kiem', 'UserController@search')->name('user.search');
        Route::delete('del_user/{id}', 'UserController@destroy')->name('del.user');
    });

    Route::resources([
        'san-bong' => 'SanBongController',
    ]);

    Route::resources([
        'khach-hang' => 'KhachHangController',
    ]);

    Route::resources([
        'khung-gio' => 'KhungGioController',
    ]);

    Route::resources([
        'khach-hang-khung-gio' => 'KhachHangKhungGioController',
    ]);

    Route::group(['prefix' => 'khach-hang-khung-gio'], function () {
//        Route::get('danh-sach', 'SanBongController@index')->name('sanbong.index');
//        Route::get('them-moi', 'SanBongController@create')->name('sanbong.create');
        Route::post('huy', 'KhachHangKhungGioController@cancel_all')->name('khach-hang-khung-gio.huy');
        Route::post('thanh-toan', 'KhachHangKhungGioController@confirm_payment')->name('khach-hang-khung-gio.post.payment');
        Route::get('thanh-toan/{id}', 'KhachHangKhungGioController@payment')->name('khach-hang-khung-gio.payment');
//        Route::get('cap-nhat/{id}', 'SanBongController@edit')->name('sanbong.edit');
//        Route::post('cap-nhat', 'SanBongController@update')->name('sanbong.confirm.edit');
//        Route::get('tim-kiem', 'SanBongController@search')->name('sanbong.search');
//        Route::delete('del_user/{id}', 'UserController@destroy')->name('del.sanbong');
    });

});

//Đăng nhập
Route::any('login', 'AuthController@login')->name('login');
//đăng xuất
Route::get('logout', 'AuthController@logout')->name('get.logout');

Route::get('migrate', function () {
    KhachHangKhungGio::query()->truncate();
    $period = \Carbon\CarbonPeriod::create('2020-10-10', '2020-12-31');
    $san_bongs = SanBong::all();
    $khunggios = KhungGio::all();
    $data_all = [];
    foreach ($period as $date) {
        foreach ($san_bongs as $san_bong) {
            foreach ($khunggios as $khunggio) {
                $data = [
                    'id_khung_gio' => $khunggio->id,
                    'id_san' => $san_bong->id,
                    'status' => 2,
                    'status_thanh_toan' => 2,
                    'active_date' => $date,
                    'created_at' => date('Y-m-d H:i'),
                    'updated_at' => date('Y-m-d H:i'),
                ];
                $data_all[] = $data;
            }
        }
    }

    KhachHangKhungGio::insert($data_all);
});
